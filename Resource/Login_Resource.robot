*** Setting ***
Library    AppiumLibrary
Library    BuiltIn

Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot

*** Variables ***
#Data valid
${EMAIL_LOGIN_VALID}   aniula2@yopmail.com
${PASSWORD_LOGIN_VALID}   123456
#Data valid Maternity
${EMAIL_LOGIN_VALID_MT}   aniula4@yopmail.com
${PASSWORD_LOGIN_VALID_MT}   123456
#Data valid New born
${EMAIL_LOGIN_VALID_NB}   aniula3@yopmail.com
${PASSWORD_LOGIN_VALID_NB}   123456
#Data Invalid
${EMAIL_LOGIN_INVALID}    aniul2@yopmail.com
${PASSWORD_LOGIN_INVALID}    1234567
#Email Forgot Password
${EMAIL_FORGOT_VALID}   aniula@yopmail.com
${EMAIL_FORGOT_INVALID}   aniula200@yopmail.com
#New Password for forgot PASSWORD
${PASSWORD_NEW}   1234567
${PASSWORD_CONFIRM}     1234567

*** Keywords ***
Login Valid
  Buka apps temanbumil real device
  # bila muncul onboarding
  # tampil halaman Login
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_email"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="login_button"]    ${timeout}
  #input alamat email
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_email"]    ${timeout}
  Input Value    //XCUIElementTypeTextField[@name="input_email"]    ${EMAIL_LOGIN_VALID}
  Click Element    //XCUIElementTypeStaticText[@name="Welcome!"]
  # input password
  Wait Until Page Contains Element    //XCUIElementTypeSecureTextField[@name="input_password"]    ${timeout}
  Input Value    //XCUIElementTypeSecureTextField[@name="input_password"]    ${PASSWORD_LOGIN_VALID}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # klik login
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="login_button"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="login_button"]
  # masuk ke halaman login
  Permission Calendar
  Sleep    1s
  Permission Photo
  Sleep    1s
  Coachmark Handle Got It
  Promo_handle

Login Invalid Email
  Buka apps temanbumil real device
  # tampil halaman Login
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_email"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="login_button"]    ${timeout}
  #input alamat email
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_email"]    ${timeout}
  Input Value    //XCUIElementTypeTextField[@name="input_email"]    ${EMAIL_LOGIN_INVALID}
  Click Element    //XCUIElementTypeStaticText[@name="Welcome!"]
  # input password
  Wait Until Page Contains Element    //XCUIElementTypeSecureTextField[@name="input_password"]    ${timeout}
  Input Value    //XCUIElementTypeSecureTextField[@name="input_password"]    ${PASSWORD_LOGIN_VALID}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # klik login
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="login_button"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="login_button"]
  # alert invalid email
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Email tidak ditemukan, periksa kembali penulisan email Anda"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]


Login Invalid Password
  Buka apps temanbumil real device
  # tampil halaman Login
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_email"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="login_button"]    ${timeout}
  #input alamat email
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_email"]    ${timeout}
  Input Value    //XCUIElementTypeTextField[@name="input_email"]    ${EMAIL_LOGIN_VALID}
  Click Element    //XCUIElementTypeStaticText[@name="Welcome!"]
  # input password
  Wait Until Page Contains Element    //XCUIElementTypeSecureTextField[@name="input_password"]    ${timeout}
  Input Value    //XCUIElementTypeSecureTextField[@name="input_password"]    ${PASSWORD_LOGIN_INVALID}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # klik login
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="login_button"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="login_button"]
  # alert invalid email
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Login gagal. Silakan periksa password kamu!"]    ${timeout}
  Page Should Contain Element    //XCUIElementTypeButton[@name="Ok"]
  Click Element    //XCUIElementTypeButton[@name="Ok"]

Lupa Password Valid
  Buka apps temanbumil real device
  # tampil halaman Login
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="forgot_password"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="login_button"]    ${timeout}
  # masuk ke page lupa Password
  Click Element    //XCUIElementTypeButton[@name="forgot_password"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Forgot password"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_email"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="ganti kata sandi"]   ${timeout}
  # input email
  Input Value    //XCUIElementTypeTextField[@name="input_email"]    ${EMAIL_ALREADY}
  Click Element    //XCUIElementTypeStaticText[@name="Forgot password"]
  Click Element    //XCUIElementTypeButton[@name="ganti kata sandi"]
  # ke halaman input kode verifikasi
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Sukses mengganti kata sandi!"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="OK"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="OK"]
  # tampil halaman verifikasi
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Email Verification"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="LANJUT"]    ${timeout}
  # klik verifikasi akun tanpa input code
  Click Element    //XCUIElementTypeButton[@name="LANJUT"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="The Code field is required."]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]

Lupa Password Invalid
  Buka apps temanbumil real device
  # tampil halaman Login
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="forgot_password"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="login_button"]    ${timeout}
  # masuk ke page lupa Password
  Click Element    //XCUIElementTypeButton[@name="forgot_password"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Forgot password"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_email"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="ganti kata sandi"]   ${timeout}
  # input email
  Input Value    //XCUIElementTypeTextField[@name="input_email"]    ${EMAIL_LOGIN_INVALID}
  Click Element    //XCUIElementTypeStaticText[@name="Forgot password"]
  Click Element    //XCUIElementTypeButton[@name="ganti kata sandi"]
  # alert
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Permintaan tidak ditemukan, silakan periksa kembali."]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]

Login New Born
  Buka apps temanbumil real device
  # bila muncul onboarding
  # tampil halaman Login
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_email"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="login_button"]    ${timeout}
  #input alamat email
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_email"]    ${timeout}
  Input Value    //XCUIElementTypeTextField[@name="input_email"]    ${EMAIL_LOGIN_VALID_NB}
  Click Element    //XCUIElementTypeStaticText[@name="Welcome!"]
  # input password
  Wait Until Page Contains Element    //XCUIElementTypeSecureTextField[@name="input_password"]    ${timeout}
  Input Value    //XCUIElementTypeSecureTextField[@name="input_password"]    ${PASSWORD_LOGIN_VALID_NB}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # klik login
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="login_button"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="login_button"]
  # masuk ke halaman login
  Sleep    3s
  Permission Calendar
  Sleep    1s
  Permission Photo
  Sleep    1s
  Coachmark Handle Got It
  Promo_handle

Promo_handle
  ${cek promo}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeButton[@name="close_redeempoin"]   8s
  Run Keyword If    ${cek promo}    Click Element    //XCUIElementTypeButton[@name="close_redeempoin"]
  Run Keyword Unless    ${cek promo}    Log    Tidak ada poster promo


Login Maternity
  Buka apps temanbumil real device
  # bila muncul onboarding
  # tampil halaman Login
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_email"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="login_button"]    ${timeout}
  #input alamat email
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_email"]    ${timeout}
  Input Value    //XCUIElementTypeTextField[@name="input_email"]    ${EMAIL_LOGIN_VALID_MT}
  Click Element    //XCUIElementTypeStaticText[@name="Welcome!"]
  # input password
  Wait Until Page Contains Element    //XCUIElementTypeSecureTextField[@name="input_password"]    ${timeout}
  Input Value    //XCUIElementTypeSecureTextField[@name="input_password"]    ${PASSWORD_LOGIN_VALID_MT}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # klik login
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="login_button"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="login_button"]
  # masuk ke halaman login
  Sleep    3s
  Permission Calendar
  Sleep    1s
  Permission Photo
  Sleep    1s
  Coachmark Handle Got It
  Promo_handle
