*** Setting ***
Library    AppiumLibrary
Library    BuiltIn

*** Variables ***
${timeout}    60s
*** Keywords ***
Permission Notifications
  ${per_notif}  Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Allow"]   15s
  Run Keyword If    ${per_notif}    Click Element    //XCUIElementTypeButton[@name="Allow"]
  ...   ELSE   Log    Permission sudah di setujui
  # ${cekupdate}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Teman Bumil"]
  # Run Keyword If    ${cekupdate}    Click Element    //XCUIElementTypeButton[@name="Tidak"]

Permission Calendar
  ${per_calendar}  Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="“Teman Bumil” Would Like to Access Your Calendar"]   15s
  Run Keyword If    ${per_calendar}    Click Element    //XCUIElementTypeButton[@name="OK"]
  ...   ELSE   Log    Permission sudah di setujui

Permission Photo
  ${per_photo}  Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="“Teman Bumil” Would Like to Access Your Photos"]    15s
  Run Keyword If    ${per_photo}    Click Element    //XCUIElementTypeButton[@name="OK"]
  ...   ELSE   Log    Permission sudah di setujui

Coachmark Handle Got It
  ${coachmark}  Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeButton[@name="GOT IT"]
  Run Keyword If    ${coachmark}    Click Element    //XCUIElementTypeButton[@name="GOT IT"]
  ...   ELSE   Log    tidak ada Coachmark

Onboarding Handle
  ${onboarding}   Run Keyword And Return Status    Wait Until Page Contains Element    accessibility_id=SKIP
  Run Keyword If    ${onboarding}    Click Element    accessibility_id=SKIP

Update Handle
  ${cekupdate}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Teman Bumil"]   8s
  Run Keyword If    ${cekupdate}    Click Element    //XCUIElementTypeButton[@name="Tidak"]
