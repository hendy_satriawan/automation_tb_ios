*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot
Resource    ../Resource/Question_Resource.robot

*** Variables ***
${nama-tambah-anak-sudah-lahir}   Ani sudah lahir

*** Keywords ***
Hapus Data Semua Program
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="side_menu"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="side_menu"]
  #cek capability
  # tampil menu pilih profile
  Coachmark Handle Got It
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Home"]   ${timeout}
  ${cekmenu}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@label,'Ani Ina2')]
  Log    ${cekmenu}
  #kalau fitur fertil -- else newborn
  Run Keyword If    ${cekmenu}    Click Element    //XCUIElementTypeStaticText[contains(@label,'Ani Ina2')]
  ...   ELSE IF   '${cekmenu}' == 'False'   Click Element    //XCUIElementTypeStaticText[@name="Ani Ula Tiga "]
  #kalau fitur Maternity
  # Run Keyword Unless    ${cekmenu}    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Ani Maternity"]    ${timeout}
  # Run Keyword Unless    ${cekmenu}    Click Element    //XCUIElementTypeStaticText[@name="Ani Maternity"]
  # masuk ke halaman profile & hapus semua data
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Profil"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="option"]    ${timeout}
  : FOR    ${loopCount}    IN RANGE    0    20
  \    Click Element    //XCUIElementTypeButton[@name="option"]
  \    Wait Until Element Is Visible    //XCUIElementTypeButton[@name="Delete"]   ${timeout}
  \    Click Element    //XCUIElementTypeButton[@name="Delete"]
  \    Wait Until Element Is Visible    //XCUIElementTypeButton[@name="Ya"]     ${timeout}
  \    Click Element    //XCUIElementTypeButton[@name="Ya"]
  \    Wait Until Element Is Visible    //XCUIElementTypeButton[@name="Ok"]     ${timeout}
  \    Click Element    //XCUIElementTypeButton[@name="Ok"]
  \    ${data}    Run Keyword And Return Status    Wait Until Page Does Not Contain Element    //XCUIElementTypeButton[@name="option"]   10s
  \    log    ${data}
  \    Run Keyword If    ${data}    Exit For Loop
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    10s


Kembali Dari Profile Ke Homepage Newborn
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="side_menu"]     ${timeout}
  Click Element    //XCUIElementTypeButton[@name="side_menu"]
  #pilih home
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Home"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Home"]
  #cek halaman homepage new born
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="notif_button"]     ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="side_menu"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${nama_anak_newborn}"]   ${timeout}

Kembali Dari Profile Ke Homepage Maternity
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="side_menu"]     ${timeout}
  Click Element    //XCUIElementTypeButton[@name="side_menu"]
  #pilih home
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Home"]   ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Home"]
  #popup selamat sudah hamil
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Selamat atas kehamilan Mums!"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="TUTUP"]
  Click Element    //XCUIElementTypeButton[@name="TUTUP"]
  #cek halaman homepage new born
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="notif_button"]     ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="side_menu"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="KEHAMILAN MUMS"]   ${timeout}


Hapus Data Tambah Anak New Born
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="side_menu"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="side_menu"]
  # masuk ke profile
  Coachmark Handle Got It
  ${cekmenu}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[contains(@label,'Ani Ina2')]
  Log    ${cekmenu}
  #kalau fitur fertil -- else newborn
  Run Keyword If    ${cekmenu}    Click Element    //XCUIElementTypeStaticText[contains(@label,'Ani Ina2')]
  ...   ELSE IF   '${cekmenu}' == 'False'   Click Element    //XCUIElementTypeStaticText[@name="Ani Ula Tiga "]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="${nama_tambah_anak_newborn}"]    ${timeout}
  # hapus data tambah Anak
  Click Element    //XCUIElementTypeStaticText[@name="${nama_tambah_anak_newborn}"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Delete"]     ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Delete"]
  # konfirmasi hapus
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Yakin di hapus?"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ya"]
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Proses berhasil!"]     ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  Wait Until Page Does Not Contain Element    //XCUIElementTypeStaticText[@name="${nama_tambah_anak_newborn}"]    ${timeout}
