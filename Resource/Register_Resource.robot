*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Library    FakerLibrary
Library    String

Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Verifikasi_Resource.robot

*** Variables ***
#Data valid
${NAMA_DEPAN}   yunia4
${NAMA_BELAKANG}    sintian1
${EMAIL_VALID}    yunsina2@yopmail.com
${EMAIL_VALID_2}    yunsin2@yopmail.com
${PASS_VALID}   123456
#Data Invalid
${EMAIL_INVALID_1}    yun@yopmail
${EMAIL_INVALID_2}    yunyopmail.com
${EMAIL_ALREADY}      aniula@yopmail.com
${PASS_INVALID}   12345
${password_regis}     123456

*** Keywords ***
Register Data Valid
  Buka apps temanbumil real device
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="login_button"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Daftar disini"]    ${timeout}
  # create data dummy
  # create first name
  ${firstname}=    First Name Female
  ${firstname}=   Set Variable    [test] ${firstname}
  Log    ${firstname}
  # create last name
  ${lastname}=  Last Name Female
  Log    ${lastname}
  # create email yang belum ada agar muncul tombol daftar
  ${count}=  Numerify   text=####
  ${emailnew}=  Set Variable    ${lastname}test${count}@yopmail.com
  Convert To Lowercase    ${emailnew}
  Log    ${emailnew}
  Set Global Variable    ${emailnew}
  # login dengan email & password yang belum pernah terdaftar
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_email"]   ${timeout}
  Input Text    //XCUIElementTypeTextField[@name="input_email"]    ${emailnew}
  Click Element    //XCUIElementTypeStaticText[@name="Welcome!"]
  # input password login
  Wait Until Page Contains Element    //XCUIElementTypeSecureTextField[@name="input_password"]    ${timeout}
  Input Text    //XCUIElementTypeSecureTextField[@name="input_password"]    ${password_regis}
  Click Element    //XCUIElementTypeStaticText[@name="Welcome!"]
  # klik login
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="login_button"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="login_button"]
  # alert belum terdaftar
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Email tidak ditemukan, periksa kembali penulisan email Anda"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Ok"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  # segera daftar
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="segera_daftar_button"]     ${timeout}
  Click Element    //XCUIElementTypeButton[@name="segera_daftar_button"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="register_button"]    ${timeout}
  # input nama depan
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_firstname"]   ${timeout}
  Input Text    //XCUIElementTypeTextField[@name="input_firstname"]   ${firstname}
  Click Element    //XCUIElementTypeStaticText[@name="Atau masuk melalui"]
  # input nama belakang
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_lastname"]    ${timeout}
  Input Text    //XCUIElementTypeTextField[@name="input_lastname"]    ${lastname}
  Click Element    //XCUIElementTypeStaticText[@name="Atau masuk melalui"]
  # input alamat email
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_email"]   ${timeout}
  Input Text    //XCUIElementTypeTextField[@name="input_email"]    ${emailnew}
  Click Element    //XCUIElementTypeStaticText[@name="Atau masuk melalui"]
  # input password
  Wait Until Page Contains Element    //XCUIElementTypeSecureTextField[@name="input_password"]    ${timeout}
  Input Text    //XCUIElementTypeSecureTextField[@name="input_password"]    ${password_regis}
  Click Element    //XCUIElementTypeStaticText[@name="Atau masuk melalui"]
  # pilih setuju
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="agree"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="agree"]
  # Click register
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="register_button"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="register_button"]
  # cek berhasil klik register
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Email Verification"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="LANJUT"]     ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Kirim ulang email verifikasi"]     ${timeout}
  ## Verifikasi Akun Via Browser Safari


Masuk Register Via Login
  # bila muncul onboarding
  # tampil halaman Login
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="login_button"]    ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Daftar disini"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Daftar disini"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="register_button"]   ${timeout}
  # cek syarat & ketentuan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="tnc"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="tnc"]
  # masuk ke halaman
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Syarat & Ketentuan"]   ${timeout}
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="close"]   ${timeout}
  # kembali ke halaman login
  Click Element    //XCUIElementTypeButton[@name="close"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="register_button"]   ${timeout}
  Sleep    2s



Register Email Sudah Terdaftar
  Buka apps temanbumil real device
  #masuk ke halaman register
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Daftar disini"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Daftar disini"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="register_button"]    ${timeout}
  # input nama depan
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_firstname"]   ${timeout}
  Input Value    //XCUIElementTypeTextField[@name="input_firstname"]    ${NAMA_DEPAN}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Atau masuk melalui"]
  # input nama belakang
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_lastname"]    ${timeout}
  Input Value    //XCUIElementTypeTextField[@name="input_lastname"]    ${NAMA_BELAKANG}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Atau masuk melalui"]
  # input alamat email
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_email"]   ${timeout}
  Input Value    //XCUIElementTypeTextField[@name="input_email"]    ${EMAIL_ALREADY}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Atau masuk melalui"]
  # input password
  Wait Until Page Contains Element    //XCUIElementTypeSecureTextField[@name="input_password"]   ${timeout}
  Input Value    //XCUIElementTypeSecureTextField[@name="input_password"]    ${PASS_VALID}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # Click Element    //XCUIElementTypeStaticText[@name="Atau masuk melalui"]
  # click setuju dengan ketentuan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="agree"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="agree"]
  # Click register
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="register_button"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="register_button"]
  # alert email sudah terdaftar
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Email anda sudah terdaftar, silakan login"]    ${timeout}
  Page Should Contain Element    //XCUIElementTypeButton[@name="Ok"]
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  Sleep    5s

Register Email Tidak Valid 1
  Buka apps temanbumil real device
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Daftar disini"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Daftar disini"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="register_button"]
  # input nama depan
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_firstname"]   ${timeout}
  Input Value    //XCUIElementTypeTextField[@name="input_firstname"]    ${NAMA_DEPAN}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Atau masuk melalui"]
  # input nama belakang
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_lastname"]    ${timeout}
  Input Value    //XCUIElementTypeTextField[@name="input_lastname"]    ${NAMA_BELAKANG}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Atau masuk melalui"]
  # input alamat email
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_email"]   ${timeout}
  Input Value    //XCUIElementTypeTextField[@name="input_email"]    ${EMAIL_INVALID_1}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Atau masuk melalui"]
  # input password
  Wait Until Page Contains Element    //XCUIElementTypeSecureTextField[@name="input_password"]   ${timeout}
  Input Value    //XCUIElementTypeSecureTextField[@name="input_password"]    ${PASS_VALID}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # Click Element    //XCUIElementTypeStaticText[@name="Atau masuk melalui"]
  # click setuju dengan ketentuan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="agree"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="agree"]
  # Click register
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="register_button"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="register_button"]
  # alert email tidak valid
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Masukkan email yang valid"]    ${timeout}
  Page Should Contain Element    //XCUIElementTypeButton[@name="Ok"]
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  Sleep    5s

Register Email Tidak Valid 2
  Buka apps temanbumil real device
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Daftar disini"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Daftar disini"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="register_button"]
  # input nama depan
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_firstname"]   ${timeout}
  Input Value    //XCUIElementTypeTextField[@name="input_firstname"]    ${NAMA_DEPAN}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Atau masuk melalui"]
  # input nama belakang
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_lastname"]    ${timeout}
  Input Value    //XCUIElementTypeTextField[@name="input_lastname"]    ${NAMA_BELAKANG}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Atau masuk melalui"]
  # input alamat email
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_email"]   ${timeout}
  Input Value    //XCUIElementTypeTextField[@name="input_email"]    ${EMAIL_INVALID_1}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Atau masuk melalui"]
  # input password
  Wait Until Page Contains Element    //XCUIElementTypeSecureTextField[@name="input_password"]   ${timeout}
  Input Value    //XCUIElementTypeSecureTextField[@name="input_password"]    ${PASS_VALID}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # Click Element    //XCUIElementTypeStaticText[@name="Atau masuk melalui"]
  # click setuju dengan ketentuan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="agree"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="agree"]

  # Click register
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="register_button"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="register_button"]
  # alert email tidak valid
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Masukkan email yang valid"]    ${timeout}
  Page Should Contain Element    //XCUIElementTypeButton[@name="Ok"]
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  Sleep    5s

Register Password Tidak Valid
  Buka apps temanbumil real device
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Daftar disini"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Daftar disini"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="register_button"]
  # input nama depan
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_firstname"]   ${timeout}
  Input Value    //XCUIElementTypeTextField[@name="input_firstname"]    ${NAMA_DEPAN}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Atau masuk melalui"]
  # input nama belakang
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_lastname"]    ${timeout}
  Input Value    //XCUIElementTypeTextField[@name="input_lastname"]    ${NAMA_BELAKANG}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Atau masuk melalui"]
  # input alamat email
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_email"]   ${timeout}
  Input Value    //XCUIElementTypeTextField[@name="input_email"]    ${EMAIL_VALID}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Atau masuk melalui"]
  # input password
  Wait Until Page Contains Element    //XCUIElementTypeSecureTextField[@name="input_password"]   ${timeout}
  Input Value    //XCUIElementTypeSecureTextField[@name="input_password"]    ${PASS_INVALID}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # Click Element    //XCUIElementTypeStaticText[@name="Atau masuk melalui"]
  # click setuju dengan ketentuan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="agree"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="agree"]

  # Click register
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="register_button"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="register_button"]
  # alert password minimal 6 huruf
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Password minimal 6 huruf"]    ${timeout}
  Page Should Contain Element    //XCUIElementTypeButton[@name="Ok"]
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  Sleep    5s

Register Tidak Pilih Setuju
  Buka apps temanbumil real device
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Daftar disini"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Daftar disini"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="register_button"]
  # input nama depan
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_firstname"]   ${timeout}
  Input Value    //XCUIElementTypeTextField[@name="input_firstname"]    ${NAMA_DEPAN}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Atau masuk melalui"]
  # input nama belakang
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_lastname"]    ${timeout}
  Input Value    //XCUIElementTypeTextField[@name="input_lastname"]    ${NAMA_BELAKANG}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Atau masuk melalui"]
  # input alamat email
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_email"]   ${timeout}
  Input Value    //XCUIElementTypeTextField[@name="input_email"]    ${EMAIL_VALID_2}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Atau masuk melalui"]
  # input password
  Wait Until Page Contains Element    //XCUIElementTypeSecureTextField[@name="input_password"]   ${timeout}
  Input Value    //XCUIElementTypeSecureTextField[@name="input_password"]    ${PASS_VALID}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # Click Element    //XCUIElementTypeStaticText[@name="Atau masuk melalui"]
  # Click register
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="register_button"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="register_button"]
  # alert password minimal 6 huruf
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Setuju syarat dan ketentuan"]    ${timeout}
  Page Should Contain Element    //XCUIElementTypeButton[@name="Ok"]
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  Sleep    5s

Register Tidak Input Mandatory Field
  Buka apps temanbumil real device
  Permission Notifications
  #masuk ke halaman register
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Daftar disini"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="Daftar disini"]
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="register_button"]
  # input nama depan
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_firstname"]   ${timeout}
  Input Value    //XCUIElementTypeTextField[@name="input_firstname"]    ${NAMA_DEPAN}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Atau masuk melalui"]
  # input nama belakang
  Wait Until Page Contains Element    //XCUIElementTypeTextField[@name="input_lastname"]    ${timeout}
  Input Value    //XCUIElementTypeTextField[@name="input_lastname"]    ${NAMA_BELAKANG}
  # Click Element    //XCUIElementTypeButton[@name="Next:"]
  Click Element    //XCUIElementTypeStaticText[@name="Atau masuk melalui"]
  # input password
  Wait Until Page Contains Element    //XCUIElementTypeSecureTextField[@name="input_password"]   ${timeout}
  Input Value    //XCUIElementTypeSecureTextField[@name="input_password"]    ${PASS_VALID}
  Click Element    //XCUIElementTypeButton[@name="Done"]
  # Click Element    //XCUIElementTypeStaticText[@name="Atau masuk melalui"]
  # click setuju dengan ketentuan
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="agree"]    ${timeout}
  Click Element    //XCUIElementTypeButton[@name="agree"]

  # Click register
  Wait Until Page Contains Element    //XCUIElementTypeButton[@name="register_button"]   ${timeout}
  Click Element    //XCUIElementTypeButton[@name="register_button"]
  # alert email sudah terdaftar
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Masukkan email yang valid"]    ${timeout}
  Page Should Contain Element    //XCUIElementTypeButton[@name="Ok"]
  Click Element    //XCUIElementTypeButton[@name="Ok"]
  Sleep    5s

Register Dengan Akun Facebook
