*** Setting ***
Library    BuiltIn
Library    String
Library    AppiumLibrary
# Library    SeleniumLibrary
Resource    ../Resource/Register_Resource.robot
*** Variables ***
${Browser}    Safari
${url}    http://www.yopmail.com/en/

${REMOTE_URL}       http://0.0.0.0:4723/wd/hub
${bundleId_saf}   com.apple.mobilesafari
${version_saf}    12.1
${deviceName_saf}    iphone 7 Plus
${udid_saf}    C4D26399-52D4-4118-B641-0107D993289D
${platformName_saf}    iOS
${automationName_saf}    XCUITest

*** Keywords ***
Verifikasi Akun Via Browser Safari
  Open Application    ${REMOTE_URL}    platformName=${platformName_saf}    platformVersion=${version_saf}    deviceName=${deviceName_saf}    bundleId=${bundleId_saf}   udid=${udid_saf}
  ...    automationName=${automationName_saf}     noReset=False     useNewWDA=True    waitForQuiescence=false
  # cek kalau harus re submit
  ${resubmit}  Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Are you sure you want to submit this form again?"]   10s
  Run Keyword If    ${resubmit}    Click Element    //XCUIElementTypeButton[@name="Cancel"]
  ...   ELSE   Log    Tidak diminta resubmit
  # masuk ke safari
  Sleep    3s
  ${cek_url}    Run Keyword And Return Status    Wait Until Page Contains Element    //XCUIElementTypeOther[@name="URL"]
  Run Keyword If    ${cek_url}    Run Keywords    Input Text    //XCUIElementTypeOther[@name="URL"]    ${url}
  ...   AND   Click Element    //XCUIElementTypeButton[@name="Go"]
  Run Keyword Unless    ${cek_url}    Wait Until Page Contains Element    //XCUIElementTypeOther[@name="Address"]     ${timeout}
  ...   AND   Input Text    //XCUIElementTypeOther[@name="Address"]    ${url}
  ...   AND   Click Element    //XCUIElementTypeButton[@name="Go"]
  # input email
  Wait Until Page Contains Element    //XCUIElementTypeOther[@name="YOPmail for mobile - Disposable Email Address"]/XCUIElementTypeOther[3]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeTextField     ${timeout}
  Clear Text    //XCUIElementTypeOther[@name="YOPmail for mobile - Disposable Email Address"]/XCUIElementTypeOther[3]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeTextField
  Input Text    //XCUIElementTypeOther[@name="YOPmail for mobile - Disposable Email Address"]/XCUIElementTypeOther[3]/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeTextField    ${emailnew}
  Click Element    //XCUIElementTypeButton[@name="Go"]
  # buka aplikasi teman bumil
  Wait Until Page Contains Element    //XCUIElementTypeLink[contains(@name,'Teman Bumil Verifikasi Akun')]    ${timeout}
  Click Element    //XCUIElementTypeLink[contains(@name,'Teman Bumil Verifikasi Akun')]
  # verifikasi akun
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Verifikasi Akun Anda"]     ${timeout}
  Click Element    //XCUIElementTypeStaticText[@name="Verifikasi Akun Anda"]
  # berhasil
  Sleep    10s
  Permission Calendar
  Permission Photo
  Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="Silahkan pilih Fitur yang Mums inginkan"]    ${timeout}
  # # buka browser
  # SeleniumLibrary.Open Browser    ${url}    ${Browser}
  # Maximize Browser Window
  # Set Browser Implicit Wait    5s
  # # input alamat email di yopmail lalu klik
  # SeleniumLibrary.Input Text    name=login     Coxtest9977@yopmail.com       #${emailnew}
  # SeleniumLibrary.Click Element    class=sbut
  # # klik verifikasi
  # # SeleniumLibrary.Wait Until Page Contains Element    //a[contains(text(),'Verifikasi Akun Anda')]
  # SeleniumLibrary.Click Element    //text()="Verifikasi Akun Anda"
  # Sleep    10s
  # Close Browser
