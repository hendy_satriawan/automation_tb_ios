*** Setting ***
Library    AppiumLibrary
Library    BuiltIn

Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Register_Resource.robot
Resource    ../Resource/Login_Resource.robot
Resource    ../Resource/Home_Resource.robot
Resource    ../Resource/Question_Resource.robot
Resource    ../Resource/DrawerMenu_Resource.robot
Resource    ../Resource/Profile_Resource.robot

# [Teardown]  Close Application

*** Test Cases ***
1.Masuk Register Via Login
  [Tags]    Masuk ke halaman Register dengan klik tombol daftar dihalaman login
  Buka apps temanbumil real device
  Masuk Register Via Login
  Close Application

2.Register Valid
  [Tags]    Buat akun Teman Bumil dengan data asli (menggunakan library faker)
  Register Data Valid
  Close Application
  Verifikasi Akun Via Browser Safari
  Close Application

3.Register Email Already
  [Tags]    Register dengan menggunakan email yang sudah terdaftar
  Register Email Sudah Terdaftar
  Close Application

4.Register Email Invalid 1
  [Tags]    Register dengan menggunakan email yang tidak valid (tanpa .com)
  Register Email Tidak Valid 1
  Close Application

5.Register Email Invalid 2
  [Tags]    Register dengan menggunakan email yang tidak valid (tanpa @)
  Register Email Tidak Valid 2
  Close Application

6.Register Password Invalid
  [Tags]    Register dengan menggunakan Password yang tidak valid (kurang dari 6)
  Register Password Tidak Valid
  Close Application

7.Register Not Click Agreement
  [Tags]    Register dengan tidak memilih setuju pada syarat & ketentuan yang berlaku
  Register Tidak Pilih Setuju
  Close Application

8.Register No Input Mandatory Field
  [Tags]    Register dengan tidak input mandatory field (tidak input alamat email)
  Register Tidak Input Mandatory Field
  Close Application

# 9.Register via Facebook Halaman Login
#   [Tags]    Register dengan akun facebook dihalaman login
#
# 10.Register via Google Halaman Login
#   [Tags]    Register dengan akun google dihalaman login
#
11.Login Data Valid
  [Tags]    Login dengan data login yang valid
  Login Valid
  Coachmark Handle Got It
  Wait Until Page Contains Element    //XCUIElementTypeImage[@name="nav_temanbumil"]    ${timeout}
  Close Application

12.Login Data Email Invalid
  [Tags]    Login dengan data login email invalid
  Login Invalid Email
  Close Application

13.Login Data Password Invalid
  [Tags]    Login dengan data login password invalid
  Login Invalid Password
  Close Application

14.Lupa Password Email Valid
  [Tags]    Melakukan lupa password dengan email valid
  Lupa Password Valid
  Close Application

15.Lupa Password Email Invalid
  [Tags]    Melakukan lupa password dengan email tidak valid
  Lupa Password Invalid
  Close Application

16.Program Hamil Homepage
  [Tags]    Login lalu create fertil (sebelumnya hapus dahulu data anak yang ada)
  Login Valid
  Question Program Hamil
  Coachmark Handle Got It
17.Program Hamil Artikel Slider 1
  [Tags]    membuka artikel slider dihomepage fertil & kembali lagi ke home
  Homepage Program Hamil Artikel Slider 1
  Kembali Ke Homepage Fertil Dari Artikel Slider
18.Program Hamil Artikel Slider 2
  [Tags]    membuka artikel slider dihomepage fertil & kembali lagi ke home
  Homepage Program Hamil Artikel Slider 2
  Kembali Ke Homepage Fertil Dari Artikel Slider
19.Program Hamil checklist
  [Tags]    checklist & unchecklist list yang ada di homepage, lalu masuk ke dalam see more & checklist lagi. lalu kembali ke homepage fertil
  Homepage Program Hamil Checklist
  Kembali Ke Homepage Fertil Dari Checklist
20.Program Hamil Tips
  [Tags]    membuka tips di homepage, scroll sampai bawah lalu kembali ke homepgae fertil
  Homepage Program Hamil Tips
  Kembali Ke Homepage Fertil Dari Tips
21.Program Hamil Tips Slide
  [Tags]    slide tips ke tips yang paling ujung di homepage, lalu buka tips & kembali lagi ke homepage fertil
  Homepage Program Hamil Tips Slide
  Kembali Ke Homepage Fertil Dari Tips
22.Program Hamil Artikel Homepage
  [Tags]    membuka artikel yang ada di homepage, scroll sampai bawah lalu kembali lagi ke homepage fertil
  Homepage Program Hamil Artikel
  Kembali Ke Homepage Fertil Dari Artikel
23.Program Hamil Checklist Notifikasi
  [Tags]    buka notifikasi, pilih lengkapi checklist & masuk ke halaman checklist. lalu kembali lagi ke homepage fertil dengan klik drawer menu home
  Homepage Program Hamil Checklist Notifikasi
  Kembali Ke Homepage Fertil Dari Checklist Notifikasi
24.Program Hamil Drawer Menu
  [Tags]    test pada semua fitur yang ada di drawer menu
  Log    Drawer Menu
25.Program Hamil Drawer Menu Checklist
  [Tags]    checklist & unchecklist di checklist dari drawer menu checklist
  Program Hamil Menu Checklis
26.Program Hamil Drawer Menu Agenda
  [Tags]    create agenda, Open,complete & uncomplete, share & delete
  Program Hamil Menu Agenda
27.Program Hamil Drawer Menu Album
  [Tags]    create album, foto, edit album & hapus
  Program Hamil Menu ALbum
28.Program Hamil Drawer Menu Artikel - belum selesai
  [Tags]    Open artikel, artikel terkait, search artikel
  Program Hamil Menu Artikel
  Kembali ke List Artikel
  Search Artikel Program Hamil
  Kembali ke List Artikel Dari Search
29.Program Hamil Drawer Menu Tips
  [Tags]    Open tips, tips terkait, search tips
  Program Hamil Menu Tips
  Kembali ke list tips
  Search Tips Program Hamil
  Kembali ke List Tips Dari Cari
  Kembali Ke Homepage Fertil Dari Drawer Menu
### digunakan bila maternity sudah selesai dibuat & berhasil dijalankan
# 30.Ubah Status Menjadi Sedang Hamil
#   [Tags]    kembali ke Home & pilih sudah hamil
#   Ubah status Menjadi Sudah Hamil
# 31.Homepage Sedang Hamil Janin 3D
#   [Tags]    Buka Janin 3D & scroll kebawah
#   Sedang Hamil Homapage Janin 3D
  Logout Akun
  Sleep    10s
32.New Born Homepage
  [Tags]    Login dengan akun lain, lalu create new born
  Login New Born
  Question Newborn
33.Homepage New Born Foto Baby
  [Tags]    upload foto baby di homepage new born
  Homepage New Born Foto Baby
  Cek upload Foto & Fitur This Week
34.Homepage New Born Record Baby
  [Tags]    buka halaman record baby lalu kembali ke homepage newborn
  Record baby homepage newborn
  Kembali ke Home Newborn dari Add Record
35.Homepage New Born Album
  [Tags]    buka halaman album lalu kembali ke homepage newborn
  Add Album Hompeage NewBorn
  Kembali ke Home Newborn dari Add Album
36.Homepage Info Tumbuh Kembang
  [Tags]    masuk ke halaman info tumbuh kembang anak lalu kembali lagi ke homepage new born
  Homepage New Born Info Tumbuh Kembang
  Kembali ke Home Newborn dari Info Tumbuh Kembang
37.Homepage Checklist New Born
  [Tags]    cari bagian checklist, check pada checklist yang ada di home klik see more lalu kembali lagi ke homepage new born
  Homepage New Born Checklist
  Kembali ke Home Newborn dari Checklist
38.Homepage Tips New Born
  [Tags]    cari tips & buka tips detail
  Homepage New Born Tips
  Kembali ke Homepage New Born dari Detail Tips
39.Homepage Artikel New Born
  [Tags]    cari Artikel & buka artikel detail
  Homepage New Born Artikel
  Kembali ke Homepage New Born dari Detail Artikel
40.Homepage Notifikasi Checklist New Born
  [Tags]    masuk ke halaman notifikasi lalu masuk ke halaman checklist
  Homepage New Born Notifikasi Checklist
  Kembali ke Homepage New Born dari checklist Notifikasi
41.New Born Drawer Menu
  [Tags]    test pada semua fitur yang ada di drawer menu
  Log    Drawer Menu New Born
42.New Born Drawer Menu Checklist
  [Tags]    masuk ke dalam menu checklist via side menu lalu lakukan checklist
  Drawer Menu Chekclist New Born
43.New Born Drawer Menu Agenda
  [Tags]    masuk ke dalam menu agenda via side menu lalu lakukan input agenda
  Drawer Menu Agenda New Born
44.New Born Drawer Menu Grafik Anak
  [Tags]    masuk ke dalam menu grafik anak via side menu lalu lakukan input data grafik
  Drawer Menu Grafik Anak New Born
  Kembali ke Home Grafik New Born
  Edit Data Janin New Born
45.New Born Drawer Menu Album
  [Tags]    masuk ke dalam menu Album via side menu lalu buat album
  Drawer Menu Album New Born
46.New Born Drawer Menu Artikel
  [Tags]    masuk kedalama menu artikel lalu filter dan buka detail artikel, dan lakukan pencarian artikel
  Drawer Menu Artikel New Born
  Kembali ke List Artikel
  Cari artikel New Born
  Kembali ke List Artikel dari Cari
47.New Born Drawer Menu Tips
  [Tags]    masuk kedalama menu tips lalu filter dan buka detail artikel, buka tips lalu lakukan pencarian tips
  Drawer Menu Tips New Born
  Kembali ke List Tips New Born
  Cari Tips New Born
  Kembali ke List Tips New Born dari Cari
48.New Born Drawer Menu Forum
  [Tags]    masuk kedalam menu forum lalu create forum, open detail forum, cari thread & bookmark
  Drawer Menu Forum
  Create Thread Forum New Born
  Kembali ke list Forum Dari Create Thread
  Open Detail Thread Forum
  Kembali ke list Forum Dari Reply Thread
  Cari thread
  Kembali ke list Forum Dari Reply Thread
  Buka Bookmark Thread
  Kembali ke List Forum Dari Bookmark
49.New Born Drawer Menu Resep
  [Tags]    masuk kedalam menu resep lalu open resep, cari resep & bookmark
  Drawer Menu Resep New Born
  Buka Resep
  Kembali ke list resep
  Cari Resep
  Kembali ke list resep
  Buka Bookmark Resep
50.New Born Tambah Anak
  [Tags]    Tambah Anak
  Tambah Anak New Born
  Hapus Data Tambah Anak New Born
  Kembali Dari Profile Ke Homepage Newborn

# 30.Maternity Drawer Menu
#   [Tags]    Login dengan akun lain, lalu create maternity
#   Login Maternity
#   Question Maternity
# 32.Maternity Drawer Menu
#   [Tags]    test pada semua fitur yang ada di drawer menu
#   Log    Drawer Menu Maternity
# 32A.Maternity Drawer Menu Checklist
#   [Tags]    masuk ke dalam menu checklist via side menu lalu lakukan checklist
#   Drawer Menu Chekclist Maternity
# 32B.Maternity Drawer Menu Agenda
#   [Tags]    masuk ke dalam menu agenda via side menu lalu lakukan input agenda
#   Drawer Menu Agenda Maternity
# 32C.Maternity Record Mom Baby
#   [Tags]    masuk ke dalam menu Record Mom & Baby via side menu lalu lakukan input data grafik
#   Drawer Menu Record Mom Baby
#   Kembali ke Home Record Mom Maternity
#   # Record Janin
# 32D.Maternity Drawer Menu Album
#   [Tags]    masuk ke dalam menu Album via side menu lalu buat album
#   Drawer Menu Album Maternity
# 32E.Maternity Drawer Menu Artikel
#   [Tags]    masuk kedalama menu artikel lalu filter dan buka detail artikel, dan lakukan pencarian artikel
#   Drawer Menu Artikel Maternity
#   Kembali ke List Artikel
#   Cari artikel Maternity
#   Kembali ke List Artikel dari Cari
# 32F.Maternity Drawer Menu Tips
#   [Tags]    masuk kedalama menu tips lalu filter dan buka detail artikel, buka tips lalu lakukan pencarian tips
#   Drawer Menu Tips Maternity
#   Kembali ke List Tips Maternity
#   Cari Tips Maternity
#   Kembali ke List Tips Maternity dari Cari
# 32G.Maternity Tambah Anak
#   [Tags]    Tambah Anak dimaternity
#   Tambah Anak Maternity
#   Hapus Data Tambah Anak New Born
#   Kembali Dari Profile Ke Homepage Newborn
